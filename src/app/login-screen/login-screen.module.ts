import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { LoginComponent } from '../login/login.component';
import {MatFormFieldModule} from '@angular/material/form-field'
import {MatInputModule} from '@angular/material/input';
import { UserService } from '../user.service';


@NgModule({
  declarations: [
    LoginComponent
  ],
  imports: [
    CommonModule,
    MatFormFieldModule,
    MatInputModule,
  ],
  exports:[
    LoginComponent,
    MatFormFieldModule,
    MatInputModule,
  ],
  providers:[UserService]
})
export class LoginScreenModule { }
