import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';


import { MainDashComponent } from '../main-dash/main-dash.component';
import { NavbarComponent } from '../navbar/navbar.component';
import { LoginComponent } from '../login/login.component';
import { AdminComponent } from '../admin/admin.component';
import { Module1Component } from '../module1/module1.component';

const routes: Routes = [
   
    { path: '', 
    component: NavbarComponent,
    
    children:[
        { path: '',pathMatch:"full",children:[
            {path:'',component:MainDashComponent}
           
        ]}, 
        {path:'test',component:Module1Component},
        { path: 'admin', component: AdminComponent},
        { path: 'login',children:[
            {path:"",component: LoginComponent}
        ]},

       
        
        //{ path: '', component: AdminComponent, outlet: "sub"},
        
            ],
    },
  
];
@NgModule({
    imports: [RouterModule.forChild(routes)],
    exports: [RouterModule]
})
export class MainDashRoutingModule { }
