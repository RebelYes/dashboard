import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import {MatFormFieldModule} from '@angular/material/form-field'
import {MatInputModule} from '@angular/material/input';
import { LoginScreenModule } from './login-screen/login-screen.module';
import { NavbarComponent } from './navbar/navbar.component';
import { LayoutModule } from '@angular/cdk/layout';
import { MatToolbarModule } from '@angular/material/toolbar';
import { MatButtonModule } from '@angular/material/button';
import { MatSidenavModule } from '@angular/material/sidenav';
import { MatIconModule } from '@angular/material/icon';
import { MatListModule } from '@angular/material/list';
import { MainDashComponent } from './main-dash/main-dash.component';
import { MatGridListModule } from '@angular/material/grid-list';
import { MatCardModule } from '@angular/material/card';
import { MatMenuModule } from '@angular/material/menu'
import { MainDashboardModule } from './main-dashboard/main-dashboard.module';
import { AdminComponent } from './admin/admin.component';
import { Module1Component } from './module1/module1.component';
@NgModule({
  declarations: [
        AppComponent,
        AdminComponent,
        Module1Component,
      //  MainDashComponent
    
    
   
  

  ],
  imports: [
    BrowserModule,
    
    BrowserAnimationsModule,
    MatFormFieldModule,
      MatInputModule,
      LoginScreenModule,
      LayoutModule,
      MatToolbarModule,
      MatButtonModule,
      MatSidenavModule,
      MatIconModule,
      MatListModule,
      MatGridListModule,
      MatCardModule,
      MatMenuModule,
      MainDashboardModule,
      AppRoutingModule,
  ],
  exports:[
    MatFormFieldModule,
      MatInputModule,
      LoginScreenModule,
      MatFormFieldModule,
      MatInputModule,
      LayoutModule,
      MatToolbarModule,
      MatButtonModule,
      MatSidenavModule,
      MatIconModule,
      MatListModule,
      MatGridListModule,
      MatCardModule,
      MatMenuModule,
      MainDashboardModule
      
  
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
