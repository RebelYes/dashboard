import { Component, OnInit } from '@angular/core';
import {UserService} from '../user.service'

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {

  constructor(
    private userService: UserService
  ) { }

  ngOnInit(): void {
  }
  login(data){
    console.log(data)
    this.userService.loginUser(data).then(x=>{
      console.log(x)
    });
    //console.log("Logged In")
  }

}
